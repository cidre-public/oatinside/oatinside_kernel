#include <asm/parallelperms.h>

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS

#include <linux/syscalls.h>
#include <asm/mmu_context.h>
#include <asm/pgalloc.h>
#include <asm/page.h>

#include <asm/system_misc.h>

void show_pte_b(struct mm_struct *mm, unsigned long addr);

SYSCALL_DEFINE1(pp_switch_perms, unsigned long, mode)
{
	struct thread_info *thread = current_thread_info();
	struct task_struct *task = thread->task;
	struct mm_struct *mm = task->mm;

	down_write(&mm->mmap_sem);

	if (unlikely(!mm->pgd_b))
		if (!pgd_alloc_b(mm))
			goto out_of_mem;

	if (mode == PP_SWITCH_TO_AS_A) {
		if (task->active_pgd == mm->pgd_a)
			goto no_change;
		/* printk("Switching to A\n"); */
		task->active_pgd = mm->pgd_a;
		goto switched;

	} else if (mode == PP_SWITCH_TO_AS_B) {
		if (task->active_pgd == mm->pgd_b)
			goto no_change;
		/* printk("Switching to B\n"); */
		task->active_pgd = mm->pgd_b;
		goto switched;
	}

	up_write(&mm->mmap_sem);
	return -PP_ERR_INVALID_REQUESTED_MODE;

 no_change:
	up_write(&mm->mmap_sem);
	return PP_SUCCESS_NO_CHANGE;

 switched:
	task->pgd_switched = true;
        switch_mm(task->mm, task->mm, task, task);
        up_write(&mm->mmap_sem);
        return mode;

 out_of_mem:
	up_write(&mm->mmap_sem);
	return -PP_ERR_OUTOFMEM;
}


SYSCALL_DEFINE3(pp_mprotect_b, unsigned long, addr, size_t, length, unsigned long, prot)
{
	/* unsigned int index; */
	pmd_t *pgd_a, *pgd_b;
	pud_t *pud_a, *pud_b;
	pmd_t *pmd_a, *pmd_b;
	pte_t *pte_a, *pte_b;
	/* unsigned long *pte_b_perms; */

	pud_t* new_pudt = NULL;
	pmd_t* new_pmdt = NULL;
	pte_t* new_ptet = NULL;
	
	/* pgtable_t new_pgtable = NULL; */
	/* unsigned long *new_second_level_perms = NULL; */

	/* spinlock_t *ptl = NULL; */
	struct vm_area_struct *vma = NULL;

	struct thread_info *thread = current_thread_info();
	struct task_struct *task = thread->task;
	struct mm_struct *mm = task->mm;

	unsigned long max_addr = addr + length;

	/* printk("Mprotect %p (%zu) -> %p\n", addr, length, max_addr); */

	down_write(&mm->mmap_sem);
	
	for(;addr<max_addr;addr+=PAGE_SIZE) {

	  /* printk("\t%p %i\n", addr, max_addr-addr); */

	  if (unlikely(!mm->pgd_b))
	    if (!pgd_alloc_b(mm))
	      goto out_of_mem;
	  if (addr >= TASK_SIZE)
	    goto invalid_addr;

	  addr &= PAGE_MASK;
	  vma = find_extend_vma(mm, addr);
	  if (!vma || addr < vma->vm_start)
	    goto invalid_addr;

	  pgd_a = mm->pgd_a + pgd_index(addr);
	  if (pgd_none(*pgd_a) || pgd_bad(*pgd_a)) printk("Error 1\n");
	  pud_a = pud_offset(pgd_a, addr);
	  /* if (pud_none(*pud_a) || pud_bad(*pud_a)) printk("Error 2\n"); */
	  if (unlikely(pud_none(*pud_a)) && __pmd_alloc(mm, pud_a, addr))
	    goto out_of_mem;	  
	  /* pud_a = pud_offset(pgd_a, addr); */
	  if (pud_none(*pud_a) || pud_bad(*pud_a)) printk("Error 2\n");

	  // Get a reference to the entry in top level table A that corresponds to
	  // the address to protect.
	  /* index = pmd_index(addr); */
	  /* index = pgd_index(addr); */
	  pmd_a = pmd_offset(pud_a, addr);
	  /* pmd_a = (pmd_t*)(pgd_a) + index; */
	  /* pmd_a = (pmd_t*)(mm->pgd_a + index); */
	  /* pmd_a = __pte_map(pmd_offset(mm->pgd_a, addr)); */
	  /* pmd_a = (pmd_t*)(pud_page_vaddr(*(pud_t*)mm->pgd_a) + index); */

	  // If the entry does not point to a second level page table, allocate a
	  // new second level page table and populate the top level of table A.
	  if (unlikely(pmd_none(*pmd_a)) && __pte_alloc(mm, vma, pmd_a, addr))
	    goto out_of_mem;

	  /* if (pmd_trans_huge(*pmd_a)) */
	  /*   printk("pmd_trans_huge\n"); */

	  // Get references to the second level tables in A table.
	  pte_a = pte_offset_map(pmd_a, addr);

	  // Get references to the second level tables in B table.
	  pgd_b = mm->pgd_b + pgd_index(addr);
	  if (pgd_none(*pgd_b) || pgd_bad(*pgd_b)) printk("Error 3\n");
	  if ((pgd_val(*pgd_b) & PMD_TYPE_MASK) == PMD_TYPE_SECT) printk("PGD_TYPE_SECT\n");
	  pud_b = pud_offset(pgd_b, addr);
	  if (pud_none(*pud_b) || pud_bad(*pud_b)) printk("Error 4\n");
	  if ((pud_val(*pud_b) & PMD_TYPE_MASK) == PMD_TYPE_SECT) printk("PUD_TYPE_SECT\n");
	  pmd_b = pmd_offset(pud_b, addr);
	  if (pmd_none(*pmd_b) || pmd_bad(*pmd_b)) printk("Error 5\n");
	  if (pmd_sect(*pmd_b)) printk("PMD_TYPE_SECT\n");
	  /* pmd_b = (pmd_t*)(pud_b + index); */
	  /* pmd_b = (pmd_t*)(pud_page_vaddr(*(pud_t*)mm->pgd_b) + index); */
	  pte_b = pte_offset_map(pmd_b, addr);

	  /* printk("pmd*=%llu %llu\n", pmd_a, pmd_b); */
	  /* printk("pmd=%llu %llu\n", pmd_val(*pmd_a), pmd_val(*pmd_b)); */
	  /* printk("pte*=%llu %llu\n", pte_a, pte_b);	 */
	  /* printk("pte_a=%llu %llu\n", pte_val(*pte_a), pte_val(*pte_b)); */
	
	  /* // Allocate a separate second level page table for the B table in the */
	  /* // event both the A and B tables are pointing to the same second level */
	  /* // table. We require a separate table to hold a different set of */
	  /* // protection bits. */
	  /* if (unlikely(pte_a == pte_b)) { */
	  /* 	new_pgtable = pte_alloc_one(mm, addr); */
	  /* 	if (!new_pgtable) */
	  /* 		goto out_of_mem; */

	  /* 	/\* new_second_level_perms = pp_perms_second_alloc(); *\/ */
	  /* 	/\* if (!new_second_level_perms) { *\/ */
	  /* 	/\* 	pte_free(mm, new_pgtable); *\/ */
	  /* 	/\* 	goto out_of_mem; *\/ */
	  /* 	/\* } *\/ */
	  /* 	/\* memset(new_second_level_perms, 0, PP_PERMS_SECOND_SIZE); *\/ */
	  /* 	// Copy everything from the second level of table A to the newly */
	  /* 	// allocated page table. */
	  /* 	/\* memcpy((void*)new_pgtable, __pte_map(pmd_a), PTE_TABLE_SIZE); *\/ */
	  /* 	/\* memcpy((void*)__va(new_pgtable), __pte_map(pmd_a), PTE_TABLE_SIZE); *\/ */
	  /* 	/\* memcpy((void*)page_to_virt(new_pgtable), __pte_map(pmd_a), PTE_TABLE_SIZE); *\/ */
	  /* 	memcpy((void*)__va(page_to_pfn(new_pgtable)<<PAGE_SHIFT), __pte_map(pmd_a), PTE_TABLE_SIZE); */
	  /* 	smp_wmb(); */

	  /* 	spin_lock(&mm->page_table_lock); */
	  /* 	/\* pte_a = pte_offset_map(pmd_a, addr); *\/ */
	  /* 	/\* pte_b = pte_offset_map(pmd_b, addr); *\/ */
	  /* 	if (likely(pte_a == pte_b)) { */
	  /* 		pmd_populate_nosync(mm, pmd_b, new_pgtable); */
	  /* 		new_pgtable = NULL; */

	  /* 		/\* index = PP_PERMS_TOP_INDEX(addr); *\/ */
	  /* 		/\* mm->pp_perms[index] = new_second_level_perms; *\/ */
	  /* 	} */
	  /* 	spin_unlock(&mm->page_table_lock); */

	  /* 	if (new_pgtable) { */
	  /* 		pte_free(mm, new_pgtable); */
	  /* 		/\* pp_perms_second_free(new_second_level_perms); *\/ */
	  /* 	} */
	  /* } */

	  if (unlikely(pte_a == pte_b)) {	      	    
	    // Alloc new pages
	    // TODO: We have to check if high levels are already allocated

	    if(pud_a == pud_b) {
	      /* new_pudt = pud_alloc(mm, pgd_b, addr); */
	      new_pudt = get_zeroed_page(GFP_KERNEL);
	      if (!new_pudt) goto out_of_mem;
	      /* printk("pud_n=%p pud_a=%p\n",new_pudt,pud_a); */
	    }

	    if(pmd_a == pmd_b) {
	      /* new_pmdt = pmd_alloc_one(mm, addr); */
	      new_pmdt = get_zeroed_page(GFP_KERNEL);
	      if (!new_pmdt) {pud_free(mm, new_pud); goto out_of_mem;}
	      /* printk("pmd_n=%p pmd_a=%p\n",new_pmdt,pmd_a); */
	    }
		
	    /* new_ptet = pte_alloc_one(mm, addr); */
	    new_ptet = get_zeroed_page(GFP_KERNEL);
	    if (!new_ptet) {pud_free(mm, new_pudt); pmd_free(mm, new_pmdt); goto out_of_mem;}
	    /* printk("pte_n=%p pte_a=%p\n",new_ptet,pte_a); */

	    /* printk("show_pte\n"); */
	    /* show_pte(mm, addr); */
	    /* printk("show_pte_b\n"); */
	    /* show_pte_b(mm, addr); */

	    // Copy old pages

	    if(pud_a == pud_b) {
	      /* printk("%p %p %p\n", __va(pgd_val(*pgd_a)), ((unsigned long long)__va(pgd_val(*pgd_a))) & ~PAGE_SIZE, ((unsigned long long)__va(pgd_val(*pgd_a))) & ~3);	 */
	      memcpy(new_pudt, ((unsigned long long)__va(pgd_val(*pgd_a))) & ~3, PAGE_SIZE);
	    }
		
	    if(pmd_a == pmd_b) {
	      /* printk("%p %p %p\n", __va(pud_val(*pud_a)), ((unsigned long long)__va(pud_val(*pud_a))) & ~PAGE_SIZE, ((unsigned long long)__va(pud_val(*pud_a))) & ~3); */
	      memcpy(new_pmdt,((unsigned long long)__va(pud_val(*pud_a))) & ~3, PAGE_SIZE);
	    }
		
	    /* printk("%p %p %p\n", __va(*pmd_a), ((unsigned long long)__va(*pmd_a)) & ~PAGE_SIZE, ((unsigned long long)__va(*pmd_a)) & ~3); */
	    memcpy(new_ptet, ((unsigned long long)__va(*pmd_a)) & ~3, PTE_TABLE_SIZE);
	    /* *new_ptet = pte_val(*pte_a); */
	    smp_wmb();

	    /* *new_ptet = 0x42424242; */
	    /* *pte_offset_map(new_ptet, addr) = 0x42424242; */

	    // Set new pages
	    spin_lock(&mm->page_table_lock);

	    /* TODO: switch to 'nosync' version of populate functions */
		
	    if(pud_a == pud_b) {
	      pgd_populate(mm, pgd_b, new_pudt);
		
	      pud_b = pud_offset(pgd_b, addr);
	      /* pud_populate(mm, pud_b, new_pmdt, addr); */
	    }
		
	    if(pmd_a == pmd_b) {
	      set_pud(pud_b, __pud(__pa(new_pmdt) | PMD_TYPE_TABLE));
	      pmd_b = pmd_offset(pud_b, addr);
	      /* pmd_populate_kernel(mm, pmd_b, __pa(new_ptet));	 */
	    }

	    pmd_populate_kernel(mm, pmd_b, new_ptet);
	    pte_b = pte_offset_map(pmd_b, addr);
		
	    spin_unlock(&mm->page_table_lock);

	    /* printk("show_pte\n"); */
	    /* show_pte(mm, addr); */
	    /* printk("show_pte_b\n"); */
	    /* show_pte_b(mm, addr); */
	  }

	  /* printk("pte_b=%p\n",pte_b); */

	  /* printk("Now set the perms\n"); */

	  /* pte_a = pte_offset_map_lock(mm, pmd_a, addr, &ptl); */

	  /* index = PP_PERMS_TOP_INDEX(addr); */
	  /* pte_b_perms = mm->pp_perms[index]; */

	  /* index = PP_PERMS_SECOND_INDEX(addr); */
	  /* pte_b_perms[index] = prot; */

	  /* printk("pmd=%llu\n    %llu\n", pmd_val(*pmd_b), pmd_val(*pmd_a)); */
	  /* set_pmd_at(mm, addr, pmd_a, pte_modify(pmd_val(*pmd_a), vm_get_page_prot(prot))); */
	  /* set_pmd_at(mm, addr, pmd_b, pte_modify(pmd_val(*pmd_b), vm_get_page_prot(prot))); */
	  /* /\* set_pmd_at(mm, addr, pmd_b, pmd_val(*pmd_b) | PTE_WRITE | PTE_RDONLY | PTE_UXN); *\/ */
	  /* /\* set_pmd_at(mm, addr, pmd_a, pmd_val(*pmd_a) | PTE_WRITE | PTE_RDONLY | PTE_UXN); *\/ */
	  /* flush_tlb_range(vma, addr, addr + PAGE_SIZE); */
	  /* printk("pmd=%llu\n    %llu\n", pmd_val(*pmd_b), pmd_val(*pmd_a)); */

	  /* printk("pmd_a=%p pmd_b=%p\n",pmd_a, pmd_b); */
	  /* printk("a=%llu b=%llu\n", pmd_val(*pmd_a), pmd_val(*pmd_b)); */
	
	  /* printk("pte=%llu\n", pte_val(*pte_b)); */
	  /* set_pte_at(mm, addr, pte_a, pte_modify(pte_val(*pte_a), vm_get_page_prot(prot))); */

	  /* set_pte_at(mm, addr, pte_a, pte_val(*pte_a) | PTE_NG); */
	  /* set_pte_at(mm, addr, pte_b, pte_modify(pte_val(*pte_b), vm_get_page_prot(prot)) | PTE_NG); */

	  /* printk("pmd_a=%llu\n", pmd_val(*pmd_a)); */
	  /* printk("pte_a=%llu\n", pte_val(*pte_a)); */

	  /* set_pte_at(mm, addr, pte_a, pte_val(*pte_a)); */
	  /* set_pte_at(mm, addr, pte_a, pte_modify(pte_val(*pte_a), vm_get_page_prot(prot)) | PTE_TYPE_PAGE); */

	  /* printk("%llu %llu\n", pte_val(*pte_a), pte_val(*pte_b)); */

	  /* THE FOLLOWING IS THE GOOD ONE */
	  set_pte_at_nosync(mm, addr, pte_b, pte_modify(pte_val(*pte_b), vm_get_page_prot(prot)) | PTE_TYPE_PAGE);

	  /* printk("pte_b=%llu\n", pmd_val(*pmd_b)); */

	  /* set_pte_at(mm, addr, pte_b, mk_pte(virt_to_page(addr), vm_get_page_prot(prot)) | PTE_TYPE_PAGE); */

	  /* printk("pmd_b=%llu\n", pmd_val(*pmd_b)); */
	  /* printk("pte_b=%llu\n", pte_val(*pte_b)); */

	  /* printk("pmd_a=%llu\n", pmd_val(*pmd_a)); */
	  /* printk("pte_a=%llu\n", pte_val(*pte_a)); */

	  /* set_pte_at_nosync(mm, addr, pte_b, pte_val(*pte_b) | PTE_WRITE | PTE_RDONLY | PTE_UXN); */
	  /* set_pte_at_nosync(mm, addr, pte_a, pte_val(*pte_a) | PTE_WRITE | PTE_RDONLY | PTE_UXN); */
	
	  /* pmd_populate_nosync(mm, pmd_a, NULL); */
	  /* *(pmd_page_vaddr(*(pmd_a)) + pte_index(addr)) = NULL */
	
	  flush_tlb_range(vma, addr, addr + PAGE_SIZE);
	  flush_cache_range(vma, addr, addr + PAGE_SIZE);


	  /* if(addr == 0x12c00000) { */
	  /* printk("show_pte\n"); */
	  /* show_pte(mm, addr); */
	  /* /\* printk("show_pte_b\n"); *\/ */
	  /* show_pte_b(mm, addr); */
	  /* } */

	  /* asm("mrs %0, tcr_el1" : "=r" (reg)); */
	  /* printk("AS=%llu ; A1=%llu\n", (reg&(_AT(u64, 1)<<36))>>36, (reg&(1<<22))>>22); */
	  
	  /* printk("a=%llu b=%llu\n", pte_val(*pte_a), pte_val(*pte_b)); */
	  /* printk("pte_a=%p pte_b=%p\n",pte_a, pte_b); */

	  /*
	   * set_pte() does not have a DSB for user mappings, so make sure that
	   * the page table write is visible.
	   */
	  dsb(ishst);
	}
	/* show_pte_b(mm, 0x12c00000); */
	goto success;
 out_of_mem:
	/* printk("Out of mem\n"); */
	up_write(&mm->mmap_sem);
	return -PP_ERR_OUTOFMEM;

 invalid_addr:
	/* printk("Invalid addr\n"); */
	up_write(&mm->mmap_sem);
	return -PP_ERR_INVALID_ADDRESS;

 success:
	/* pte_unmap_unlock(pte_a, ptl); */
	/* printk("End mprotect\n"); */
	up_write(&mm->mmap_sem);
	return prot;
}

void show_pte_b(struct mm_struct *mm, unsigned long addr)
{
	pgd_t *pgd;

	if (!mm)
		mm = &init_mm;

	pr_alert("pgd = %p\n", mm->pgd_b);
	if(mm->pgd_b) {
	  pgd = mm->pgd_b + pgd_index(addr);
	  pr_alert("[%08lx] *pgd=%016llx", addr, pgd_val(*pgd));

	  do {
	    pud_t *pud;
	    pmd_t *pmd;
	    pte_t *pte;

	    if (pgd_none(*pgd) || pgd_bad(*pgd))
	      break;

	    pud = pud_offset(pgd, addr);
	    if (pud_none(*pud) || pud_bad(*pud))
	      break;

	    pmd = pmd_offset(pud, addr);
	    printk(", *pmd=%016llx", pmd_val(*pmd));
	    if (pmd_none(*pmd) || pmd_bad(*pmd))
	      break;

	    pte = pte_offset_map(pmd, addr);
	    printk(", *pte=%016llx", pte_val(*pte));
	    pte_unmap(pte);
	  } while(0);

	  printk("\n");
	}
}

#endif
