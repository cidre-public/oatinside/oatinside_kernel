/*
 * PGD allocation/freeing
 *
 * Copyright (C) 2012 ARM Ltd.
 * Author: Catalin Marinas <catalin.marinas@arm.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/mm.h>
#include <linux/gfp.h>
#include <linux/highmem.h>
#include <linux/slab.h>

#include <asm/page.h>
#include <asm/pgalloc.h>
#include <asm/tlbflush.h>

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
#include <asm/parallelperms.h>
#endif

#include "mm.h"

#define PGD_SIZE        (PTRS_PER_PGD * sizeof(pgd_t))
#define USER_PTRS_PER_PGD	(TASK_SIZE / PGDIR_SIZE)

pgd_t *pgd_alloc(struct mm_struct *mm)
{
	if (PGD_SIZE == PAGE_SIZE)
		return (pgd_t *)get_zeroed_page(GFP_KERNEL);
	else
		return kzalloc(PGD_SIZE, GFP_KERNEL);
}

void pgd_free(struct mm_struct *mm, pgd_t *pgd)
{
	if (PGD_SIZE == PAGE_SIZE)
		free_page((unsigned long)pgd);
	else
		kfree(pgd);

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
	pgd_free_b(mm);
#endif
}

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
pgd_t *pgd_alloc_b(struct mm_struct *mm)
{
	pgd_t *init_pgd, *new_pgd;
	/* unsigned long **new_pp_perms; */
 
	new_pgd = pgd_alloc(mm);
	if (!new_pgd)
		goto no_pgd;

	/* new_pp_perms = pp_perms_top_alloc(); */
        /* if (!new_pp_perms) { */
	/* 	if (PGD_SIZE == PAGE_SIZE) */
	/* 		free_page((unsigned long)new_pgd); */
	/* 	else */
	/* 		kfree(new_pgd); */
        /*         goto no_pgd; */
        /* } */

	/*
	 * Copy over the user-space mappings
	 */
	spin_lock(&mm->page_table_lock);
	if (likely(!mm->pgd_b)) {
		mm->pgd_b = new_pgd;
		new_pgd = NULL;
		memcpy(mm->pgd_b, mm->pgd_a, PTRS_PER_PGD * sizeof(pgd_t));
		/* memcpy(mm->pgd_b, mm->pgd_a, USER_PTRS_PER_PGD * sizeof(pgd_t)); */
		/* mm->pp_perms = new_pp_perms; */
		/* memset(new_pp_perms, 0, PP_PERMS_TOP_SIZE); */
	}
	spin_unlock(&mm->page_table_lock);

	if (new_pgd) {
		if (PGD_SIZE == PAGE_SIZE)
			free_page((unsigned long)new_pgd);
		else
			kfree(new_pgd);
		/* pp_perms_top_free(new_pp_perms); */
		goto success;
	}

	/* /\* */
	/*  * Copy over the kernel and IO PGD entries */
	/*  *\/ */
	/* spin_lock(&init_mm.page_table_lock); */
	/* init_pgd = pgd_offset_k(0); */
	/* memcpy(mm->pgd_b + USER_PTRS_PER_PGD, init_pgd + USER_PTRS_PER_PGD, */
	/*        (PTRS_PER_PGD - USER_PTRS_PER_PGD) * sizeof(pgd_t)); */
	/* spin_unlock(&init_mm.page_table_lock); */

	__flush_dcache_area(mm->pgd_b, PTRS_PER_PGD * sizeof(pgd_t));

success:
	return mm->pgd_b;

no_pgd:
	return NULL;
}


void pgd_free_b(struct mm_struct *mm)
{
	/* unsigned long *perms; */
	/* pmd_t *pmd_a, *pmd_b; */
	/* pte_t *pte_a, *pte_b; */
	/* int i; */

  /* TODO: free other page level */

	if (mm->pgd_b) {
		/* for (i = 0; i < USER_PTRS_PER_PGD; i++) { */
		/* 	pmd_a = (pmd_t*)(mm->pgd_a + i); */
		/* 	pmd_b = (pmd_t*)(mm->pgd_b + i); */
		/* 	pte_a = __pte_map(pmd_a); */
		/* 	pte_b = __pte_map(pmd_b); */

		/* 	if (pte_a != pte_b) */
		/* 		pte_free(mm, virt_to_page((unsigned long)pte_b)); */
		/* } */

		if (PGD_SIZE == PAGE_SIZE)
			free_page((unsigned long)mm->pgd_b);
		else
			kfree(mm->pgd_b);

		mm->pgd_b = NULL;
	}

	/* if (mm->pp_perms) { */
	/* 	for (i = 0; i < PP_PERMS_PER_TOP_LEVEL; i++) { */
	/* 		perms = mm->pp_perms[i]; */
	/* 		if (perms) */
	/* 			pp_perms_second_free(perms); */
	/* 	} */

	/* 	pp_perms_top_free(mm->pp_perms); */
	/* } */
}
#endif
