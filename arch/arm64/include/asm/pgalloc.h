/*
 * Based on arch/arm/include/asm/pgalloc.h
 *
 * Copyright (C) 2000-2001 Russell King
 * Copyright (C) 2012 ARM Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __ASM_PGALLOC_H
#define __ASM_PGALLOC_H

#include <asm/pgtable-hwdef.h>
#include <asm/processor.h>
#include <asm/cacheflush.h>
#include <asm/tlbflush.h>

#define check_pgt_cache()		do { } while (0)

#ifndef CONFIG_ARM64_64K_PAGES

static inline pmd_t *pmd_alloc_one(struct mm_struct *mm, unsigned long addr)
{
	return (pmd_t *)get_zeroed_page(GFP_KERNEL | __GFP_REPEAT);
}

static inline void pmd_free(struct mm_struct *mm, pmd_t *pmd)
{
	BUG_ON((unsigned long)pmd & (PAGE_SIZE-1));
	free_page((unsigned long)pmd);
}

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
static inline void pud_populate(struct mm_struct *mm, pud_t *pud, pmd_t *pmd, unsigned long addr)
#else
static inline void pud_populate(struct mm_struct *mm, pud_t *pud, pmd_t *pmd)
#endif
{
#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
	pmd_t *pgd_b;
	pud_t *pud_b;
#endif
	set_pud(pud, __pud(__pa(pmd) | PMD_TYPE_TABLE));
#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
	if (mm && mm->pgd_b) {
	  pgd_b = mm->pgd_b + pgd_index(addr);
	  pud_b = pud_offset(pgd_b, addr);

	  if(!(pud_none(*pud_b) || pud_bad(*pud_b)) && pud_b != pud)
	    printk("Overwritting changed pud?\n");	  
	  set_pud(pud_b, __pud(__pa(pmd) | PMD_TYPE_TABLE));
	}
#endif
}

#endif	/* CONFIG_ARM64_64K_PAGES */

extern pgd_t *pgd_alloc(struct mm_struct *mm);
extern void pgd_free(struct mm_struct *mm, pgd_t *pgd);

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
extern pgd_t *pgd_alloc_b(struct mm_struct *mm);
extern void pgd_free_b(struct mm_struct *mm);
#endif

#define PGALLOC_GFP	(GFP_KERNEL | __GFP_NOTRACK | __GFP_REPEAT | __GFP_ZERO)

static inline pte_t *
pte_alloc_one_kernel(struct mm_struct *mm, unsigned long addr)
{
	return (pte_t *)__get_free_page(PGALLOC_GFP);
}

static inline pgtable_t
pte_alloc_one(struct mm_struct *mm, unsigned long addr)
{
	struct page *pte;

	pte = alloc_pages(PGALLOC_GFP, 0);
	if (!pte)
		return NULL;
	if (!pgtable_page_ctor(pte)) {
		__free_page(pte);
		return NULL;
	}
	return pte;
}

/*
 * Free a PTE table.
 */
static inline void pte_free_kernel(struct mm_struct *mm, pte_t *pte)
{
	if (pte)
		free_page((unsigned long)pte);
}

static inline void pte_free(struct mm_struct *mm, pgtable_t pte)
{
	pgtable_page_dtor(pte);
	__free_page(pte);
}

static inline void __pmd_populate(pmd_t *pmdp, phys_addr_t pte,
				  pmdval_t prot)
{
	set_pmd(pmdp, __pmd(pte | prot));
}

/*
 * Populate the pmdp entry with a pointer to the pte.  This pmd is part
 * of the mm address space.
 */
static inline void
pmd_populate_kernel(struct mm_struct *mm, pmd_t *pmdp, pte_t *ptep)
{
	/*
	 * The pmd must be loaded with the physical address of the PTE table
	 */
	__pmd_populate(pmdp, __pa(ptep), PMD_TYPE_TABLE);
}

static inline void
#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
pmd_populate(struct mm_struct *mm, pmd_t *pmdp, pgtable_t ptep, unsigned long addr)
#else
pmd_populate(struct mm_struct *mm, pmd_t *pmdp, pgtable_t ptep)
#endif
{
#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
	pmd_t *pgd_b;
	pud_t *pud_b;
	pmd_t *pmd_b;
	/* pmd_t *pmdp_b; */
#endif
	__pmd_populate(pmdp, page_to_phys(ptep), PMD_TYPE_TABLE);
#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
	if (mm && mm->pgd_b) {
	  /* printk("C\n"); */
		/* pmdp_b = get_other_pmd(pmdp, mm->pgd_b); */
		/* __pmd_populate(pmdp_b, page_to_phys(ptep), PMD_TYPE_TABLE); */
	  
		pgd_b = mm->pgd_b + pgd_index(addr);
		if (pgd_none(*pgd_b) || pgd_bad(*pgd_b))
		  printk("er\n");
		
		pud_b = pud_offset(pgd_b, addr);
		if (pud_none(*pud_b) || pud_bad(*pud_b))
		  printk("err\n");
		
		pmd_b = pmd_offset(pud_b, addr);

		if(!(pmd_none(*pmd_b) || pmd_bad(*pmd_b)) && pmd_b != pmdp)
		  printk("Overwritting changed pmd @%p?\n", addr);
		__pmd_populate(pmd_b, page_to_phys(ptep), PMD_TYPE_TABLE);
	}
#endif
}

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
static inline void
pmd_populate_nosync(struct mm_struct *mm, pmd_t *pmdp, pgtable_t ptep)
{
	__pmd_populate(pmdp, page_to_phys(ptep), PMD_TYPE_TABLE);
}
#endif

#define pmd_pgtable(pmd) pmd_page(pmd)

#endif
