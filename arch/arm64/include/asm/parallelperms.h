#ifndef __ASMARM_PARALLELPERMS_H
#define __ASMARM_PARALLELPERMS_H
#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS

#include <linux/types.h>

#define PP_ERR_OUTOFMEM 1
#define PP_ERR_INVALID_REQUESTED_MODE 2
#define PP_ERR_INVALID_ADDRESS 3

#define PP_SUCCESS_NO_CHANGE 0

#define PP_SWITCH_TO_AS_A 1
#define PP_SWITCH_TO_AS_B 2

#define PP_PERM_MIRROR_A	0 /* Use the same page permissions as in address
				   * space A. This must be zero.
				   */
#ifndef __ASSEMBLY__

long sys_pp_switch_perms(unsigned long mode);
long sys_pp_mprotect_b(unsigned long addr, size_t len, unsigned long prot);

// The permissions to apply in the second page table are stored in a different
// table-based data structure. This is the "permissions table" and it resembles
// a two-level page table. The top level permissions table has 1024 entries and
// each entry points to a second level table. A second level table also has 1024
// entry and each entry contains the permissions to use. The upper 10 bits of a
// virtual address are used to index into the top level table and the next 10
// bits of the address are used to index into the second level table.
#define PP_PERMS_PER_TOP_LEVEL			1024
#define PP_PERMS_TOP_LEVEL_MASK			0xFFC00000
#define PP_PERMS_SECOND_LEVEL_MASK		0x003FF000

#define PP_PERMS_TOP_LEVEL_SHIFT		22
#define PP_PERMS_SECOND_LEVEL_SHIFT		12

#define PP_PERMS_TOP_INDEX(addr)	(((addr) & PP_PERMS_TOP_LEVEL_MASK) >> PP_PERMS_TOP_LEVEL_SHIFT)
#define PP_PERMS_SECOND_INDEX(addr)	(((addr) & PP_PERMS_SECOND_LEVEL_MASK) >> PP_PERMS_SECOND_LEVEL_SHIFT)

#define PP_PERMS_TOP_SIZE	PAGE_SIZE
#define PP_PERMS_SECOND_SIZE	PAGE_SIZE

#define pp_perms_top_alloc()			(unsigned long **)__get_free_pages(GFP_KERNEL, 0)
#define pp_perms_top_free(perms)		free_pages((unsigned long)perms, 0);

#define pp_perms_second_alloc()			(unsigned long *)__get_free_pages(GFP_KERNEL, 0);
#define pp_perms_second_free(second_perms)	free_pages((unsigned long)second_perms, 0);

#endif /* __ASSEMBLY__ */
#endif /* CONFIG_PGTABLES_PARALLEL_PERMISSIONS */
#endif /* __ASMARM_PARALLELPERMS_H */
